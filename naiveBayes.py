#!/usr/bin/python
# -*- coding: utf-8
import sys

def makePredictionsRawInput(model):

  prshsh, pruhsh, default = model[0],model[1],model[2]

  kk=raw_input().decode("utf8").rstrip()
  while kk != '':
    prs = prshsh.get('s[0]='+kk,0)
    pru = pruhsh.get('s[0]='+kk,0)
    if (prs > pru):
      print "+"
    elif (pru > prs):
      print '-'
    else:
      print '0 '+default
    kk=raw_input().decode("utf8").rstrip()


def makePredictions(model, testfilename):

  prshsh, pruhsh, default = model[0],model[1],model[2]
  f=open(testfilename)
  lines=[line.decode("utf8").rstrip().split("\t") for line in f]
  f.close()

  for line in lines:
    if line!=['']:
      currSyll = line[21] #Specify the column
#    print "Syll",currSyll
#    if currSyll != '':
      prs = prshsh.get(currSyll,0)
      pru = pruhsh.get(currSyll,0)
      if (prs > pru):
        print "+"
      elif (pru > prs):
        print '-'
      else:
        print default
    else:
      print 

def trainModel(filename):
  f=open(filename)
  lines=[line.decode("utf8").rstrip().split("\t") for line in f if line.rstrip()!='']
  f.close()

  stressed={}
  unstressed={}
  probStressed={}
  probUnstressed={}
  for line in lines:
#    stress, syll = line #simple way
    #spanish format (64 feats)
    stress=line[0]
    syll=line[22]
#    print stress, syll
    #print syll
    if stress == '+':
      stressed[syll]=stressed.get(syll,0)+1
#    elif stress == '=': #English
    elif stress == '-': #Spanish
      unstressed[syll]=unstressed.get(syll,0)+1
    else:
      print ("NOOO!")
      exit()

  cStressed = sum(stressed.values())
  cUnstressed = sum(unstressed.values())
  N= cStressed + cUnstressed

  priorStressed = float(cStressed)/N
  priorUnstressed = float(cUnstressed)/N

  if priorStressed < priorUnstressed:
    default = '-'
  else:
    default = '+'

#  sys.stderr.write ("str,unstr")
#  sys.stderr.write((str(priorStressed)+","+str(priorUnstressed)+"\n"))

  for syll in stressed:
    probStressed[syll]=priorStressed*float(stressed[syll])/cStressed

  for syll in unstressed:
    probUnstressed[syll]=priorUnstressed*float(unstressed[syll])/cUnstressed

#  for syll in unstressed:
#    print syll
#    probUnstressed[syll]=priorUnstressed*float(unstressed[syll])/cUnstressed
#  print "s[0]=más".decode("utf8").encode("utf8"),
#  print probStressed['s[0]=más'.decode("utf8")],
#  print probUnstressed['s[0]=más'.decode("utf8")]
#  raw_input()

#  print probStressed.get('s[0]=to',0),probUnstressed.get('s[0]=to',0)
#  print probStressed.get('s[0]=be',0),probUnstressed.get('s[0]=be',0)

  return (probStressed,probUnstressed,default)


def main():
  model = trainModel(sys.argv[1])
#  makePredictionsRawInput(model)
  makePredictions(model, sys.argv[2])

  prst=model[0]
  prun=model[1]
#  print prst
#  for el in prun:
#    print el, prst.get(el,-1),prun.get(el,-1)

if __name__ == '__main__':
  main()

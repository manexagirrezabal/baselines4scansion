import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--corpus', help='Corpus name', required=True)
parser.add_argument('--pred', help='Print predictions at file <filename>', required=True)
parser.add_argument('--baseline', help='Baseline that we use [lex|stressed|unstressed|iambicfirst|iambiclast]', default="lex")
args = parser.parse_args()

import sys

from baselines import alwaysiambicfirstlex
from baselines import alwaysiambiclastlex
from baselines import alwaysstressed
from baselines import alwaysunstressed
from baselines import alwayslex
from baselines import syllableweight

forpredfile=[]
if args.baseline != 'iambiclast':
  f=open(args.corpus)
  lines = [line.decode("utf8").rstrip().split("\t") for line in f]
  f.close()

  #Initialization of variables
  noinstances=len([line for line in lines if line!=['']])
  y_pred=noinstances*['']
  predictedinstances=0
  syllno=0
  previous_pred=''


  for line in lines:
    if line != ['']:
      if args.baseline == "lex":
        pred = alwayslex.baseline(line, syllno)
      elif args.baseline == "stressed":
        pred = alwaysstressed.baseline()
      elif args.baseline == "unstressed":
        pred = alwaysunstressed.baseline()
      elif args.baseline == "iambicfirst":
        pred = alwaysiambicfirstlex.baseline(line, syllno, previous_pred)
      elif args.baseline == "syllableweight":
        pred = syllableweight.baseline(line)
      else:
        sys.exit("poo")

      forpredfile.append(pred.encode("utf8")+"\n")

      y_pred[predictedinstances]=pred
      previous_pred= pred
      syllno=syllno+1
      predictedinstances=predictedinstances+1
    else:
      syllno=0
      forpredfile.append("\n")

else:
  f=open(args.corpus)
  lines = [line.rstrip().split("\t") for line in f]
  f.close()

  #Initialization of variables
  noinstances=len([line for line in lines if line!=['']])
  y_pred=noinstances*['']
  predictedinstances=0
  syllno=0
  previous_pred=''


  for line in reversed(lines):
    if line != ['']:
      if args.baseline == "iambiclast":
        pred = alwaysiambiclastlex.baseline(line, syllno, previous_pred) #In order to use this baseline, add reversed in the declaration of the for loop "for line in reversed(lines):"
      else:
        sys.exit("poo")

      forpredfile.insert(0,pred.encode("utf8")+"\n")

      y_pred[predictedinstances]=pred
      previous_pred= pred
      syllno=syllno+1
      predictedinstances=predictedinstances+1
    else:
      syllno=0
      forpredfile.insert(0,"\n")



predfile=open(args.pred, "w")
for i in forpredfile:
  predfile.write(i)
predfile.close()





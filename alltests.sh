rm files/*
rm tmpres/*
#cat ~/Dropbox/poetryCorpora/formattedCorpusAllfeaturessyllswospacesGoodCV/newcorpus_ALLSTR-test-*-of-10.crf > files/newcorpus.crf
#cp ~/Dropbox/poetryCorpora/spanishpoemDataset/corpus64Features.crf files/newcorpus.crf
cp /home/magirrezaba008/basquepoemDataset/corpus64Features.crf files/newcorpus.crf
/home/magirrezaba008/Dropbox/poetryCorpora/scripts/test4ZS.sh files/newcorpus.crf

#for i in lex stressed unstressed iambicfirst iambiclast syllableweight
for i in lex iambicfirst iambiclast
do
  echo "Trying with baseline "$i
  python testbaselines.py --corpus files/newcorpus_NOSP.feats --pred tmpres/kk.preds --baseline $i
  python /home/magirrezaba008/Dropbox/poetryCorpora/scripts/crfres2ZS.py tmpres/kk.preds
  python /home/magirrezaba008/Dropbox/poetryCorpora/scripts/evaluate.py files/newcorpus_NOSP.classes tmpres/kk.sequences
done


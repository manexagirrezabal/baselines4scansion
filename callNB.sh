

#for i in 1 2 3 4 5 6 7 8 9 10
#do
#  python naiveBayes.py /home/magirrezaba008/Dropbox/poetryCorpora/formattedCorpusAllfeaturesJust1syllGoodCV/newcorpusJustClassSyll-train-${i}-of-10.crf /home/magirrezaba008/Dropbox/poetryCorpora/formattedCorpusAllfeaturesJust1syllGoodCV/newcorpusJustClassSyll-test-${i}-of-10_NOSP.feats > /home/magirrezaba008/tmp/naiveBayesoutput${i}.txt
#  python /home/magirrezaba008/Dropbox/poetryCorpora/crfres2ZS.py /home/magirrezaba008/tmp/naiveBayesoutput${i}.txt
#done

DATADIR=/home/magirrezaba008/Dropbox/poetryCorpora/spanishpoemDataset/10FoldDivision448seed/
DATADIR=/home/magirrezaba008/basquepoemDataset/10FoldTDTDivision448seed/

for i in 1 2 3 4 5 6 7 8 9 10
do
#  /home/magirrezaba008/Dropbox/poetryCorpora/scripts/test4ZS.sh /home/magirrezaba008/Dropbox/poetryCorpora/spanishpoemDataset/10FoldDivision448seed/corpus64Features-test-${i}-of-10.crf
  python naiveBayes.py ${DATADIR}/corpus64Features-train-${i}-of-10.crf ${DATADIR}/corpus64Features-test-${i}-of-10_NOSP.feats > /home/magirrezaba008/tmp/naiveBayesoutput${i}.txt
  python /home/magirrezaba008/Dropbox/poetryCorpora/scripts/crfres2ZS.py /home/magirrezaba008/tmp/naiveBayesoutput${i}.txt
done

for i in 1 2 3 4 5 6 7 8 9 10
do
  python /home/magirrezaba008/Dropbox/poetryCorpora/scripts/evaluate.py ${DATADIR}/corpus64Features-test-${i}-of-10_NOSP.classes /home/magirrezaba008/tmp/naiveBayesoutput${i}.sequences
done

for i in 1 2 3 4 5 6 7 8 9 10
do
  python /home/magirrezaba008/Dropbox/poetryCorpora/scripts/evaluate.py ${DATADIR}/corpus64Features-test-${i}-of-10_NOSP.classes /home/magirrezaba008/tmp/naiveBayesoutput${i}.sequences
done | grep "^ACCURACY"

for i in 1 2 3 4 5 6 7 8 9 10
do
  python /home/magirrezaba008/Dropbox/poetryCorpora/scripts/evaluate.py ${DATADIR}/corpus64Features-test-${i}-of-10_NOSP.classes /home/magirrezaba008/tmp/naiveBayesoutput${i}.sequences
done | grep "^LINE ACCURACY"

rm /home/magirrezaba008/tmp/*

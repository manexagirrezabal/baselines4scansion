
def map(string):
  if string=="'":
    return "+"
  elif string=="`":
    return "=" #Putting a =, gives a higher accuracy (Let's call this a parameter of the baseline OMG! :-P)
  elif string=="_":
    return "="
  else:
    raise ValueError("I didn't expect the value ("+string+") here. I was expecting ', ` or _")

def mapBasque(string):
  if string=="'":
    return "+"
  elif string=="`":
    return "=" #Putting a =, gives a higher accuracy (Let's call this a parameter of the baseline OMG! :-P)
  elif string=="-":
    return "="
  else:
    raise ValueError("I didn't expect the value ("+string+") here. I was expecting ', ` or -")

def mapSpanish(string):
  if string=="+":
    return "+"
  elif string=="?":
    return "-" #Putting a =, gives a higher accuracy (Let's call this a parameter of the baseline OMG! :-P)
  elif string=="-":
    return "-"
  else:
    raise ValueError("I didn't expect the value ("+string+") here. I was expecting +, - or ?")

def baseline (line, which):
#  if which > 0:
#    if prev == '=':
#      return '+'
#    else:
#      return '='
#  else:
  #ls=line[11][line[11].index("=")+1:].replace("m","") #English corpus
  #snow=int(line[-5][4:]) #English corpus
#  print "1",line[-11], line[0]
  ls=line[-11][line[-11].index("=")+1:].replace("m","") #Spanish corpus
  snow=int(line[0][5:]) #Spanish corpus
#  print "2",ls, line[-11], snow
#  print line[0], snow
#  raw_input()
#  print line
  if snow < len(ls):
#    print ls,snow,ls[snow], len(ls)
    lsatposition=ls[snow]
    return mapBasque(lsatposition) #Suitable for the English corpus
    #return map(lsatposition) #Suitable for the English corpus
    #return mapSpanish(lsatposition) #Suitable for the Spanish corpus
  return "l" #Putting a =, gives a higher accuracy (Let's call this another parameter of the baseline OMG! :-P)


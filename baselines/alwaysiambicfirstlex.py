

def baseline (line, which, prev):
  if which > 0:
    if prev == '-':
      return '+'
    else:
      return '-'
  else:
    ls=line[-11]
    if ls[6]=='m': #We need to check this because we add an 'M' at the beginning of the lexical stress if this is inferred using athenarhytm
      first_ls=ls[7]
    else:
      first_ls=ls[6]
    if first_ls=="+" or first_ls=="`": #(Spanish) Monosyllables -
      return '+'
    else:
      return '-'


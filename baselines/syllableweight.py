import re

#FROM FILE: /home/magirrezaba008/Dropbox/poetryCorpora/corpusReader/4B4V/newCorpusReader.py
#THAT's THE MAINTAINED FILE
#NEW VERSION
diphs=['ae', 'ai', 'au', 'ea', 'ee', 'ei', 'eo', 'eu', 'ia', 'ie', 'io', 'iu', 'oa', 'oe', 'oi', 'oo', 'ou', 'ua', 'ue', 'ui', 'uo']
#Diphthongs calculated using the program diphthong.py /home/magirrezaba008/Dropbox/thesis/unsupervisedScansion/hiawatha.txt
def isheavy(str):
    if str[-1] in 'bcdfghjklmnpqrstvxyz': #If the syllable ends in consonant, it is heavy
        return '+'
    elif re.sub("[^aeiou]","",str) in diphs: #If the nucleus (syllable without consonants) is a diphthong, it is heavy
        return '+'
    else: #Then, if neither of the previous conditions are satisfied, the syllable is light
        return '='

def map(string):
  if string=="'":
    return "+"
  elif string=="`":
    return "="
  elif string=="_":
    return "="
  else:
    raise ValueError("I didn't expect the value ("+string+") here. I was expecting ', ` or _")

def baseline (line):
#  if which > 0:
#    if prev == '=':
#      return '+'
#    else:
#      return '='
#  else:
  syllable=line[33].replace("s[0]=","")
#  print syllable, isheavy(syllable)
  return isheavy(syllable)

